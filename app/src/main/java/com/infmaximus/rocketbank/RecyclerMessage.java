package com.infmaximus.rocketbank;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.shamanland.fonticon.FontIconDrawable;
import com.shamanland.fonticon.FontIconTypefaceHolder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by InfMaximus on 12.07.2016.
 */
public class RecyclerMessage extends RecyclerView.Adapter<RecyclerMessage.ViewHolder>{

    private ArrayList<MessageContainer> containers;
    private Context context;
    private Drawable icon;
    public RecyclerMessage(Context context,ArrayList<MessageContainer> containers){
        Log.d("LOG_TAG", "RecyclerMessage ");
        this.containers = containers;
        this.context = context;

        try {

            FontIconTypefaceHolder.init(context.getAssets(), "fonts/icomoon.ttf");
            icon = FontIconDrawable.inflate(context.getResources(), R.xml.ic_album);
        }catch (Exception ex){
                ex.printStackTrace();
        }
    }

    @Override
    public RecyclerMessage.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.element_recycler_message, parent, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final RecyclerMessage.ViewHolder holder, int position) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d MMMM, HH:mm");
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat verySimpleDateFormat = new SimpleDateFormat("HH:mm");
            String time;
            if(position!=0){
                if(containers.get(position).getTimeMessage().get(Calendar.DAY_OF_YEAR)==
                        containers.get(position-1).getTimeMessage().get(Calendar.DAY_OF_YEAR))
                    time = verySimpleDateFormat.format(containers.get(position).getTimeMessage().getTime());
                else{
                    if(containers.get(position).getTimeMessage().get(Calendar.DAY_OF_YEAR)==calendar.get(Calendar.DAY_OF_YEAR))
                        time = context.getResources().getString(R.string.today)+", "+
                                verySimpleDateFormat.format(containers.get(position).getTimeMessage().getTime());
                    else if(containers.get(position).getTimeMessage().get(Calendar.DAY_OF_YEAR)==(calendar.get(Calendar.DAY_OF_YEAR)-1))
                        time = context.getResources().getString(R.string.yesterday)+", "+
                                verySimpleDateFormat.format(containers.get(position).getTimeMessage().getTime());
                    else
                        time = simpleDateFormat.format(containers.get(position).getTimeMessage().getTime());
                }

            }else
                if(containers.get(position).getTimeMessage().get(Calendar.DAY_OF_YEAR)==calendar.get(Calendar.DAY_OF_YEAR))
                    time = context.getResources().getString(R.string.today)+", "+
                            verySimpleDateFormat.format(containers.get(position).getTimeMessage().getTime());
                else if(containers.get(position).getTimeMessage().get(Calendar.DAY_OF_YEAR)==(calendar.get(Calendar.DAY_OF_YEAR)-1))
                    time = context.getResources().getString(R.string.yesterday)+", "+
                            verySimpleDateFormat.format(containers.get(position).getTimeMessage().getTime());
                else
                    time = simpleDateFormat.format(containers.get(position).getTimeMessage().getTime());
            holder.timeMessage.setText(time);

            if(containers.get(position).getTypeContainer()==MessageContainer.typeText){
                holder.textMessage.setVisibility(View.VISIBLE);
                holder.textMessage.setText(containers.get(position).getTextMessage());
            }else {
                holder.textMessage.setVisibility(View.GONE);
            }


            if(containers.get(position).getTypeContainer()==MessageContainer.typeImageUri){
                holder.imageMessage.setVisibility(View.VISIBLE);
                //holder.imageMessage.setImageURI(containers.get(position).getBitmapMessageUri());


                Transformation transformation = new Transformation() {

                    @Override
                    public Bitmap transform(Bitmap bitmap) {
                        int width = holder.imageMessage.getWidth();

                        double relation = (double) bitmap.getHeight() / (double) bitmap.getWidth();
                        int targetHeight = (int) (width * relation);
                        Bitmap result = Bitmap.createScaledBitmap(bitmap, width, targetHeight, false);
                        if (result != bitmap) {
                            bitmap.recycle();
                        }
                        return result;
                    }

                    @Override
                    public String key() {
                        return "key";
                    }
                };


                Picasso.with(context)
                        .load(containers.get(position).getBitmapMessageUri())
                        .placeholder(icon)
                        .transform(transformation)
                        .into(holder.imageMessage);

            }else {
                holder.imageMessage.setVisibility(View.GONE);
            }

            if(containers.get(position).getTypeContainer()==MessageContainer.typePosition){
                holder.mapMessage.setVisibility(View.VISIBLE);
                GoogleMap thisMap = holder.googleMap;
                holder.setLocation(containers.get(position).getLocation());
                if(thisMap != null) {
                    LatLng llPosition = containers.get(position).getLocation();
                    thisMap.clear();
                    thisMap.moveCamera( CameraUpdateFactory.newLatLngZoom(llPosition , 10.0f) );
                    thisMap.addMarker(new MarkerOptions().position(llPosition).title("me"));
                }
            }else {
                holder.mapMessage.setVisibility(View.GONE);
            }


        }catch (Exception ex){
            Log.e("LOG_TAG", "onBindViewHolder "+ex.toString());
        }
    }



    @Override
    public int getItemCount() {
        //Log.d("LOG_TAG", "getItemCount ");
        return containers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback {

        RelativeLayout mainContainer;
        ImageView imageMessage;
        TextView  textMessage;
        TextView  timeMessage;
        GoogleMap googleMap;
        MapView mapMessage;

        LatLng _latLng;

        public ViewHolder(View itemView) {
            super(itemView);
            mainContainer = (RelativeLayout) itemView.findViewById(R.id.massge_layout_container);
            timeMessage = (TextView) itemView.findViewById(R.id.time_message);
            textMessage = (TextView)mainContainer.findViewById(R.id.text_element_recycler_message);
            imageMessage = (ImageView)mainContainer.findViewById(R.id.image_element_recycler_message);

            mapMessage = (MapView)mainContainer.findViewById(R.id.map_element_recycler_message);
            mapMessage.setVisibility(View.GONE);

            //MapsInitializer.initialize(context);
            //gMap.getUiSettings().setZoomControlsEnabled(true);
            //gMap.getUiSettings().setMyLocationButtonEnabled(true);
            //gMap.getUiSettings().setCompassEnabled(true);
            if (mapMessage != null)
            {
                mapMessage.onCreate(null);
                mapMessage.onResume();
                mapMessage.getMapAsync(this);
            }
        }

        public void setLocation(LatLng latLng){
            _latLng = latLng;
        }


        @Override
        public void onMapReady(GoogleMap _googleMap) {
            MapsInitializer.initialize(context);
            googleMap = _googleMap;

            if(_latLng!=null) {
                googleMap.addMarker(new MarkerOptions().position(_latLng).title("me"));
                googleMap.moveCamera( CameraUpdateFactory.newLatLngZoom(_latLng , 10.0f) );
            }
        }
    }




}
