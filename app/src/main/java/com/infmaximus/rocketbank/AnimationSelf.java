package com.infmaximus.rocketbank;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;

import java.util.ArrayList;

/**
 * Created by InfMaximus on 19.07.2016.
 */
public class AnimationSelf {

    TranslateAnimation animationTextShow;
    TranslateAnimation animationTextHide;

    TranslateAnimation animationCameraShow;
    TranslateAnimation animationCameraHide;

    TranslateAnimation animationAlbumShow;
    TranslateAnimation animationAlbumHide;

    TranslateAnimation animationGeoShow;
    TranslateAnimation animationGeoHide;

    private ArrayList<TranslateAnimation> tasShow = new ArrayList<>();
    private ArrayList<TranslateAnimation> tasHide = new ArrayList<>();
    private  ArrayList<RelativeLayout> fabsClass;
    private FloatingActionButton menuFab;

    private int duration = 100;

    public AnimationSelf(Context context, final ArrayList<RelativeLayout> fabs, FloatingActionButton menuFab){
        this.fabsClass = fabs;
        this.menuFab = menuFab;

        animationTextShow = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,0, android.view.animation.Animation.RELATIVE_TO_PARENT,0,
                android.view.animation.Animation.RELATIVE_TO_PARENT,0.13f, android.view.animation.Animation.RELATIVE_TO_PARENT,0);
        animationTextShow.setDuration(duration);
        animationTextShow.setFillAfter(false);
        animationTextShow.setAnimationListener(new android.view.animation.Animation.AnimationListener() {
            @Override
            public void onAnimationStart(android.view.animation.Animation animation) {

            }

            @Override
            public void onAnimationEnd(android.view.animation.Animation animation) {

            }

            @Override
            public void onAnimationRepeat(android.view.animation.Animation animation) {

            }
        });

        animationCameraShow = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,0, android.view.animation.Animation.RELATIVE_TO_PARENT,0,
                android.view.animation.Animation.RELATIVE_TO_PARENT,0.27f, android.view.animation.Animation.RELATIVE_TO_PARENT,0);
        animationCameraShow.setDuration(duration);
        animationCameraShow.setFillAfter(false);

        animationAlbumShow = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,0, android.view.animation.Animation.RELATIVE_TO_PARENT,0,
                android.view.animation.Animation.RELATIVE_TO_PARENT,0.41f, android.view.animation.Animation.RELATIVE_TO_PARENT,0);
        animationAlbumShow.setDuration(duration);
        animationAlbumShow.setFillAfter(false);

        animationGeoShow = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,0, android.view.animation.Animation.RELATIVE_TO_PARENT,0,
                android.view.animation.Animation.RELATIVE_TO_PARENT,0.55f, android.view.animation.Animation.RELATIVE_TO_PARENT,0);
        animationGeoShow.setDuration(duration);
        animationGeoShow.setFillAfter(false);

        tasShow.add(animationTextShow);
        tasShow.add(animationCameraShow);
        tasShow.add(animationAlbumShow);
        tasShow.add(animationGeoShow);

        animationTextHide = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,0, android.view.animation.Animation.RELATIVE_TO_PARENT,0,
                android.view.animation.Animation.RELATIVE_TO_PARENT,0, android.view.animation.Animation.RELATIVE_TO_PARENT,0.13f);
        animationTextHide.setDuration(duration);
        animationTextHide.setFillAfter(false);

        animationTextHide.setAnimationListener(new android.view.animation.Animation.AnimationListener() {
            @Override
            public void onAnimationStart(android.view.animation.Animation animation) {
            }

            @Override
            public void onAnimationEnd(android.view.animation.Animation animation) {
                for (RelativeLayout fab:fabsClass){
                    fab.setVisibility(View.GONE);
                }
            }
            @Override
            public void onAnimationRepeat(android.view.animation.Animation animation) {

            }
        });

        animationCameraHide = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,0, android.view.animation.Animation.RELATIVE_TO_PARENT,0,
                android.view.animation.Animation.RELATIVE_TO_PARENT,0, android.view.animation.Animation.RELATIVE_TO_PARENT,0.27f);
        animationCameraHide.setDuration(duration);
        animationCameraHide.setFillAfter(false);

        animationAlbumHide = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,0, android.view.animation.Animation.RELATIVE_TO_PARENT,0,
                android.view.animation.Animation.RELATIVE_TO_PARENT,0, android.view.animation.Animation.RELATIVE_TO_PARENT,0.41f);
        animationAlbumHide.setDuration(duration);
        animationAlbumHide.setFillAfter(false);

        animationGeoHide = new TranslateAnimation(
                android.view.animation.Animation.RELATIVE_TO_PARENT,0, android.view.animation.Animation.RELATIVE_TO_PARENT,0,
                android.view.animation.Animation.RELATIVE_TO_PARENT,0, android.view.animation.Animation.RELATIVE_TO_PARENT,0.55f);
        animationGeoHide.setDuration(duration);
        animationGeoHide.setFillAfter(false);

        tasHide.add(animationTextHide);
        tasHide.add(animationCameraHide);
        tasHide.add(animationAlbumHide);
        tasHide.add(animationGeoHide);
    }

    public void showAll(){


        ViewCompat.animate(menuFab)
                .rotation(45.0F)
                .withLayer()
                .setDuration(300L)
                .setInterpolator(new OvershootInterpolator(10.0F))
                .start();


        for (int i = 0;i<fabsClass.size();i++){
            fabsClass.get(i).setVisibility(View.VISIBLE);
            fabsClass.get(i).startAnimation(tasShow.get(i));
        }
    }

    public void hideAll(){
        ViewCompat.animate(menuFab)
                .rotation(0.0F)
                .withLayer()
                .setDuration(300L)
                .setInterpolator(new OvershootInterpolator(10.0F))
                .start();


        for (int i = 0;i<fabsClass.size();i++){
            fabsClass.get(i).startAnimation(tasHide.get(i));
        }
    }

}
