package com.infmaximus.rocketbank;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by InfMaximus on 17.07.2016.
 */
public class Server implements Runnable{

    public Server(Context context){
        this.context = context;
    }

    protected Context context;
    ServerSocket serverSocket;
    static final int SocketServerPORT = 8080;

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(SocketServerPORT);
            while (!Thread.currentThread().isInterrupted()) {
                Socket socket = serverSocket.accept();
                CommunicationThread  socketServerReplyThread = new CommunicationThread (
                        socket);
                Thread ct = new Thread(socketServerReplyThread);
                ct.start();
                //Log.d("LOG_TAG", "Server first call");
            }
            //Log.d("LOG_TAG", "Server exit");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class CommunicationThread implements Runnable {

        private Socket clientSocket;
        private BufferedReader input;
        public CommunicationThread(Socket clientSocket) {
            this.clientSocket = clientSocket;
            try {
                //this.input = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        public void run() {

            try {
                int bytesRead;
                ByteArrayOutputStream byteArrayOutputStream =
                        new ByteArrayOutputStream(1024);
                InputStream inputStream = clientSocket.getInputStream();
                byte[] buffer = new byte[1024];
                String inputText = "";
                boolean simpleRequest = false;
                while (!Thread.currentThread().isInterrupted()&&!simpleRequest&&!clientSocket.isClosed()) {
                    try {
                        if(inputStream.available()>0) {
                            bytesRead = inputStream.read(buffer);
                            int k = bytesRead;
                            byteArrayOutputStream.write(buffer, 0, bytesRead);
                            inputText += byteArrayOutputStream.toString("UTF-8");
                            //Log.d("LOG_TAG", "Server " + inputText);

                            if ("GET_MY_LIST".equals(inputText)) {
                                //Log.d("LOG_TAG", "Server получил запрос");
                                String allMessage = WorkDB.dbGetMessage(context);
                                PrintWriter out = new PrintWriter(new BufferedWriter(
                                        new OutputStreamWriter(clientSocket.getOutputStream())), true);

                                if (!allMessage.equals("[]")) {
                                    out.print(allMessage);
                                } else {
                                    out.print("empty");
                                }
                                out.close();
                                //Log.d("LOG_TAG", "Server send message");
                                simpleRequest = true;
                            } else {
                                WorkDB.dbPutMessage(context, inputText);
                            }
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }


}
