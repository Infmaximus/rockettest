package com.infmaximus.rocketbank;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by maximk on 19.07.2016.
 */
public class WorkDB {

    public static String keyType   = "type";
    public static String keyText   = "text";
    public static String keyUri    = "uri";
    public static String keyLat    = "lat";
    public static String keyLon    = "lon";
    public static String keyDate   = "time";

    public static void dbPutMessage(Context context,String message){
        try {
            JSONObject messageObject = new JSONObject(message);
            SQLiteDatabase db = CreateDB.getInstance(context).getWritableDatabase();
            if(messageObject.has(keyType)){
                String text = "";
                String uri  = "";
                double lat = 0;
                double lon = 0;
                long   date= 0;
                int type = messageObject.getInt(keyType);
                if(messageObject.has(keyDate)){
                    date = messageObject.getLong(keyDate);
                }
                switch (type){
                    case 1:
                        if(messageObject.has(keyText)){
                            text = messageObject.getString(keyText);
                        }

                        break;
                    case 4:
                        if(messageObject.has(keyUri)){
                            uri = messageObject.getString(keyUri);
                        }
                        break;
                    case 3:
                        if(messageObject.has(keyLat)){
                            lat = messageObject.getDouble(keyLat);
                        }
                        if(messageObject.has(keyLat)){
                            lon = messageObject.getDouble(keyLon);
                        }
                        break;
                }

                ContentValues cv = new ContentValues();
                cv.put(CreateDB.COLUMN_TEXT, text);
                cv.put(CreateDB.COLUMN_TYPE, type);
                cv.put(CreateDB.COLUMN_URI, uri);
                cv.put(CreateDB.COLUMN_LAT, lat);
                cv.put(CreateDB.COLUMN_LON, lon);
                cv.put(CreateDB.COLUMN_DATE, date);

                long countUpdate = db.insert(CreateDB.TABLE_MESSAGE,null,cv);
                if (countUpdate==-1)
                    Log.d("LOG_TAG",new SQLiteException().getMessage());

            }

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public static String dbGetMessage(Context context){
        SQLiteDatabase db = CreateDB.getInstance(context).getWritableDatabase();
        String result = "";

        try {
            JSONArray jsonArr = new JSONArray();
            Cursor c = db.query(CreateDB.TABLE_MESSAGE, null, null, null, null, null, null);
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        JSONObject jsonObj = new JSONObject();
                        for (String cn : c.getColumnNames()) {
                            if (cn.contains(CreateDB.COLUMN_TYPE))
                                jsonObj.put(keyType,c.getInt(c.getColumnIndex(cn)));
                            else if (cn.contains(CreateDB.COLUMN_DATE))
                                jsonObj.put(keyDate,c.getLong(c.getColumnIndex(cn)));
                            else if (cn.contains(CreateDB.COLUMN_URI))
                                jsonObj.put(keyUri,c.getString(c.getColumnIndex(cn)));
                            else if (cn.contains(CreateDB.COLUMN_LAT))
                                jsonObj.put(keyLat,c.getDouble(c.getColumnIndex(cn)));
                            else if (cn.contains(CreateDB.COLUMN_LON))
                                jsonObj.put(keyLon,c.getDouble(c.getColumnIndex(cn)));
                            else if (cn.contains(CreateDB.COLUMN_TEXT))
                                jsonObj.put(keyText,c.getString(c.getColumnIndex(cn)));
                        }

                        jsonArr.put(jsonObj);
                    } while (c.moveToNext());
                }
                c.close();
            } else
                Log.d("LOG_TAG","Cursor is null");
            result = jsonArr.toString();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

}
