package com.infmaximus.rocketbank;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by maximk on 19.07.2016.
 */
public class CreateDB extends SQLiteOpenHelper implements BaseColumns {

    private static CreateDB Creator;
    private static final String DATABASE_NAME = "database.db";
    private static final int DATABASE_VERSION = 1;

    //-------------------------------TABLE USER-----------------------------------------------------
    public static String TABLE_MESSAGE           = "user_table";
    public static String COLUMN_ID               = "_id";
    public static String COLUMN_TYPE             = "_type";
    public static String COLUMN_TEXT             = "_text";
    public static String COLUMN_URI              = "_uri";
    public static String COLUMN_LAT              = "_lat";
    public static String COLUMN_LON              = "_lon";
    public static String COLUMN_DATE             = "_date";
    public static String COLUMN_RESERVE          = "_reserve";

    public static final String SQL_CREATE_USER_TABLE = "CREATE TABLE "
            + TABLE_MESSAGE + " (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_TYPE + " INTEGER, "
            + COLUMN_TEXT + " TEXT, "
            + COLUMN_URI + " VARCHAR(255), "
            + COLUMN_LAT + " REAL, "
            + COLUMN_LON + " REAL, "
            + COLUMN_DATE + " INTEGER, "
            + COLUMN_RESERVE + " VARCHAR(255));";
    public static final String SQL_DELETE_USER_TABLE = "DROP TABLE IF EXISTS "
            + TABLE_MESSAGE+"";

    public static synchronized CreateDB getInstance(Context context){
        if(Creator == null)
        {
            Creator = new CreateDB(context);
        }
        return Creator;
    }

    private CreateDB (Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        Log.d("LOG_TAG", "CreateDB CreateDB");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_USER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

