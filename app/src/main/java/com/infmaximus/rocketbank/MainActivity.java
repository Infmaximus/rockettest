package com.infmaximus.rocketbank;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.shamanland.fonticon.FontIconDrawable;
import com.shamanland.fonticon.FontIconTypefaceHolder;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements GetAllListAsyncTask.Callback{

    private Context context;

    private final int PERMISSIONS_REQUEST_GET_GPS = 1;
    private final int PERMISSIONS_REQUEST_GET_CAMERA = 2;
    private final int PERMISSIONS_REQUEST_GET_EXTERNAL_STORAGE = 3;

    private RecyclerView commonList;
    private RecyclerMessage reciclerAdapter;
    private ImageButton buttonSendMessage;
    private RelativeLayout relativeSendMessage;
    private RelativeLayout relativeFloatButtons;
    private RelativeLayout relativeBackground;
    //private FloatingActionsMenu actionMenu;
    private FloatingActionButton actionMenu;

    private RelativeLayout relativeLayoutText;
    private RelativeLayout relativeLayoutCamera;
    private RelativeLayout relativeLayoutAlbum;
    private RelativeLayout relativeLayoutGeo;

    private EditText messageText;

    private ArrayList<MessageContainer> container = new ArrayList<>();
    private LatLng newPoint;


    private Animation animationHideMessageBox;
    private Animation animationShowMessageBox;
    private Animation animationShowFab;
    private Animation animationHideFab;

    private Drawable[] icons;

    private boolean textWrite = false;

    private Client c;
    private GetAllListAsyncTask gala;

    private boolean menuToogle = false;
    private boolean asyncStart = false;
    private boolean firstStart;

    private WaitTask waitTask;
    private AnimationSelf animationSelf;
    private TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3)
        {
        }
        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if(messageText.getText().toString().matches("")){
                buttonSendMessage.setImageDrawable(icons[0]);
            }else {
                buttonSendMessage.setImageDrawable(icons[5]);
            }
        }
        @Override
        public void afterTextChanged(Editable editable) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;


        relativeLayoutText = (RelativeLayout)findViewById(R.id.relative_text);
        //---------------------------Блок Инициализации view----------------------------------------

        commonList = (RecyclerView)findViewById(R.id.commonListId);
        actionMenu = (FloatingActionButton)findViewById(R.id.multiple_actions);
        final FloatingActionButton actionGeo = (FloatingActionButton)findViewById(R.id.action_geo);
        final FloatingActionButton actionAlbum = (FloatingActionButton)findViewById(R.id.action_album);
        final FloatingActionButton actionCamera = (FloatingActionButton)findViewById(R.id.action_camera);
        final FloatingActionButton actionText = (FloatingActionButton)findViewById(R.id.action_text);

        relativeLayoutCamera = (RelativeLayout)findViewById(R.id.relative_camera);
        relativeLayoutAlbum = (RelativeLayout)findViewById(R.id.relative_album);
        relativeLayoutGeo = (RelativeLayout)findViewById(R.id.relative_geo);


        ArrayList<RelativeLayout> layoutArrayList = new ArrayList<>();
        layoutArrayList.add(relativeLayoutText);
        layoutArrayList.add(relativeLayoutCamera);
        layoutArrayList.add(relativeLayoutAlbum);
        layoutArrayList.add(relativeLayoutGeo);

        animationSelf = new AnimationSelf(context,layoutArrayList,actionMenu);



        relativeSendMessage  = (RelativeLayout)findViewById(R.id.relative_write_message);
        relativeFloatButtons = (RelativeLayout)findViewById(R.id.relative_float_button);
        relativeBackground   = (RelativeLayout)findViewById(R.id.relative_background);

        buttonSendMessage = (ImageButton)relativeSendMessage.findViewById(R.id.button_send_message);
        messageText = (EditText) relativeSendMessage.findViewById(R.id.edit_input_text);
        messageText.addTextChangedListener(watcher);

        //relativeFloatButtons.setVisibility(View.GONE);
        firstStart = true;
        //тест

        newPoint = new LatLng(37,55);

        Calendar calendar = Calendar.getInstance();

        container.add(new MessageContainer(MessageContainer.typeText,"1",calendar));
        container.add(new MessageContainer(MessageContainer.typeText,"2",calendar));

        container.add(new MessageContainer(MessageContainer.typeText,"3",calendar));
        container.add(new MessageContainer(MessageContainer.typePosition,newPoint,calendar));
        container.add(new MessageContainer(MessageContainer.typeText,"5",calendar));

        reciclerAdapter = new RecyclerMessage(this,container);


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        commonList.setAdapter(reciclerAdapter);
        commonList.setLayoutManager(layoutManager);
        /*
        commonList.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {


                if(!textWrite){
                    if(asyncStart){
                        waitTask.addSec(1);
                    }else {
                        waitTask = new WaitTask();
                        waitTask.execute();
                        waitTask.addSec(1);
                    }
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
        */
        commonList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //Log.d("LOG_TAG","scroled");
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

                super.onScrollStateChanged(recyclerView, newState);
                Log.d("LOG_TAG","scrolChange "+newState);
                if(!textWrite){
                    if(asyncStart){
                        waitTask.addSec(newState);
                    }else {
                        waitTask = new WaitTask();
                        waitTask.execute();
                        waitTask.addSec(newState);
                    }
                }

            }
        });


        //---------------------------Инициализация кнопок-------------------------------------------

        actionText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideFabMenu();
                //relativeSendMessage.startAnimation(animationShowMessageBox);
                relativeFloatButtons.startAnimation(animationHideFab);
                relativeSendMessage.startAnimation(animationShowMessageBox);
                messageText.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(messageText, InputMethodManager.SHOW_IMPLICIT);
                textWrite = true;

            }
        });

        actionCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                PERMISSIONS_REQUEST_GET_CAMERA);
                    }else {
                        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(takePicture, 0);
                    }
                }else {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);
                }
            }
        });

        actionAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                PERMISSIONS_REQUEST_GET_EXTERNAL_STORAGE);

                    }else {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto, 1);
                    }
                }else {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 1);
                }
            }
        });

        actionGeo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                PERMISSIONS_REQUEST_GET_GPS);

                    }else {
                        getLocation();
                    }
                }else {
                    getLocation();
                }

            }
        });

        buttonSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                relativeSendMessage.startAnimation(animationHideMessageBox);
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(messageText.getWindowToken(), 0);
                MessageContainer mc;
                if(!messageText.getText().toString().matches("")){
                    Calendar calendar = Calendar.getInstance();
                    mc = new MessageContainer(MessageContainer.typeText,messageText.getText().toString(),calendar);
                    container.add(mc);
                    reciclerAdapter.notifyDataSetChanged();

                    sendMessageToServer(mc);

                    messageText.setText("");
                }
                textWrite = false;

                //commonList.scrollToPosition(container.size());
                commonList.scrollToPosition(commonList.getAdapter().getItemCount() - 1);
                //hideFabMenu();

            }
        });

        actionMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(menuToogle) {
                    menuToogle = false;
                    animationSelf.hideAll();
                    relativeBackground.setBackgroundColor(getResources().getColor(R.color.rocket_full_transparent));
                    actionMenu.setBackgroundTintList(getResources().getColorStateList(R.color.rocket_orange));

                }else {
                    menuToogle = true;
                    animationSelf.showAll();
                    actionMenu.setBackgroundTintList(getResources().getColorStateList(R.color.rocket_dark_orange));
                    relativeBackground.setBackgroundColor(getResources().getColor(R.color.rocket_80_transparent));
                }
            }
        });


        try {

            FontIconTypefaceHolder.init(getAssets(), "fonts/icomoon.ttf");
            icons = new Drawable[]{
                    FontIconDrawable.inflate(getResources(), R.xml.ic_send),
                    FontIconDrawable.inflate(getResources(), R.xml.ic_text),
                    FontIconDrawable.inflate(getResources(), R.xml.ic_camera),
                    FontIconDrawable.inflate(getResources(), R.xml.ic_album),
                    FontIconDrawable.inflate(getResources(), R.xml.ic_geo),
                    FontIconDrawable.inflate(getResources(), R.xml.ic_send_wait),
                    FontIconDrawable.inflate(getResources(), R.xml.ic_menu)
            };

            actionMenu.setImageDrawable(icons[6]);
            actionGeo.setImageDrawable(icons[4]);
            actionAlbum.setImageDrawable(icons[3]);
            actionCamera.setImageDrawable(icons[2]);
            actionText.setImageDrawable(icons[1]);
            buttonSendMessage.setImageDrawable(icons[0]);

            InitialisationAnimation();

        }catch (Exception ex){
            ex.printStackTrace();
        }

        //check();

        try {
            startClientAndServer();
        }catch (Exception ex){
            Log.d("LOG_TAG",ex.toString());
        }

    }

    private void check() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_REQUEST_GET_GPS);
            }
            if (checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        PERMISSIONS_REQUEST_GET_CAMERA);

            }
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        PERMISSIONS_REQUEST_GET_EXTERNAL_STORAGE);

            }
        }
    }

    public void InitialisationAnimation(){
        try {
            animationShowMessageBox = AnimationUtils.loadAnimation(this, R.anim.mytrans_ok_up);
            animationShowMessageBox.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    relativeSendMessage.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animation arg0) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            animationHideMessageBox = AnimationUtils.loadAnimation(this, R.anim.mytrans_ok_down);
            animationHideMessageBox.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation arg0) {
                    relativeSendMessage.setVisibility(View.GONE);
                    //relativeFloatButtons.setVisibility(View.VISIBLE);
                    //relativeFloatButtons.startAnimation(animationShowFab);

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            animationHideFab = AnimationUtils.loadAnimation(this, R.anim.mytrans_ok_down);
            animationHideFab.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation arg0) {
                    relativeFloatButtons.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            animationShowFab = AnimationUtils.loadAnimation(this, R.anim.mytrans_ok_up);
            animationShowFab.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    relativeFloatButtons.setVisibility(View.VISIBLE);
                    actionMenu.setBackgroundTintList(getResources().getColorStateList(R.color.rocket_orange));
                }
                @Override
                public void onAnimationEnd(Animation arg0) {
                }
                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

        }catch (Exception ex){

        }

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case 0:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    Log.d("LOG_TAG", "onActivityResult "+selectedImage);
                    Calendar calendar = Calendar.getInstance();
                    MessageContainer mc = new MessageContainer(MessageContainer.typeImageUri,selectedImage,calendar);
                    container.add(mc);
                    reciclerAdapter.notifyDataSetChanged();
                    hideFabMenu();
                    sendMessageToServer(mc);
                    //imageview.setImageURI(selectedImage);
                    commonList.scrollToPosition(commonList.getAdapter().getItemCount() - 1);
                }

                break;
            case 1:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    Log.d("LOG_TAG", "onActivityResult "+selectedImage);
                    Calendar calendar = Calendar.getInstance();
                    MessageContainer mc = new MessageContainer(MessageContainer.typeImageUri,selectedImage,calendar);
                    container.add(mc);
                    reciclerAdapter.notifyDataSetChanged();
                    hideFabMenu();
                    sendMessageToServer(mc);
                    //imageview.setImageURI(selectedImage);
                    commonList.scrollToPosition(commonList.getAdapter().getItemCount() - 1);
                }
                break;
        }
    }

    private void getLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String bestProvider = locationManager.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }



        Location location;
        LocationListener loc_listener = new LocationListener() {

            public void onLocationChanged(Location l) {}

            public void onProviderEnabled(String p) {}

            public void onProviderDisabled(String p) {}

            public void onStatusChanged(String p, int status, Bundle extras) {}
        };

        // getting GPS status
        boolean isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        // getting network status
        boolean isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        locationManager
                .requestLocationUpdates(bestProvider, 0, 0, loc_listener);
        if (!isGPSEnabled && !isNetworkEnabled) {
            Toast.makeText(context,getString(R.string.geo_not_available),Toast.LENGTH_LONG).show();
            hideFabMenu();
        }else {
            location = locationManager.getLastKnownLocation(bestProvider);
            try {
                Calendar calendar = Calendar.getInstance();
                LatLng newPosition = new LatLng(location.getLatitude(), location.getLongitude());
                MessageContainer mc = new MessageContainer(MessageContainer.typePosition, newPosition, calendar);
                container.add(mc);
                reciclerAdapter.notifyDataSetChanged();
                hideFabMenu();

                commonList.scrollToPosition(container.size());

                sendMessageToServer(mc);
                commonList.scrollToPosition(commonList.getAdapter().getItemCount() - 1);
            } catch (Exception ex) {
                Log.e("LOG_TAG", ex.toString());
            }
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_GET_GPS: {
                if ( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                    return;
                }

                break;
            }
            case PERMISSIONS_REQUEST_GET_CAMERA: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);
                    return;
                }
                break;
            }
            case PERMISSIONS_REQUEST_GET_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 1);
                    return;
                }
                break;
            }
        }
    }

    //Нажатие кнопки назад
    @Override
    public void onBackPressed() {
        if(menuToogle) {
            hideFabMenu();
        }
        else if(textWrite) {
            relativeSendMessage.startAnimation(animationHideMessageBox);
            relativeFloatButtons.startAnimation(animationShowFab);
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(messageText.getWindowToken(), 0);
            if(!messageText.getText().toString().matches("")){
                Calendar calendar = Calendar.getInstance();
                MessageContainer mc = new MessageContainer(MessageContainer.typeText,messageText.getText().toString(),calendar);
                container.add(mc);
                reciclerAdapter.notifyDataSetChanged();
            }
            textWrite = false;
        }else {
            new AlertDialog.Builder(this)
                    .setMessage(getResources().getString(R.string.sure_exit))
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(R.string.exit, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            exit();
                        }
                    })
                    .create()
                    .show();
        }
    }

    //Выход из приложения
    public void exit() {
        finish();
    }

    private void startClientAndServer(){

        /*
        ProgressDialog pd = new ProgressDialog(context);
        pd.setMessage("Запуск сервера");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
*/
        Server s = new Server(context);
        Thread serverThread = new Thread(s);
        serverThread.start();

        gala = new GetAllListAsyncTask(context);
        gala.registerCallBack(this);
        gala.execute();


    }

    public void hideFabMenu(){
        menuToogle = false;
        animationSelf.hideAll();
        actionMenu.setBackgroundTintList(getResources().getColorStateList(R.color.rocket_orange));
        relativeBackground.setBackgroundColor(getResources().getColor(R.color.rocket_full_transparent));

    }

    public String jsonToString(MessageContainer mc){
        String result = "";
        try {
        JSONObject positionJson = new JSONObject();
            positionJson.put("type", mc.getTypeContainer());
            positionJson.put("time", mc.getTimeMessage().getTimeInMillis());
            switch (mc.getTypeContainer()){
                case MessageContainer.typeText:
                    positionJson.put("text", mc.getTextMessage());
                    break;
                case MessageContainer.typeImageUri:
                    positionJson.put("uri", mc.getBitmapMessageUri());
                    break;
                case MessageContainer.typePosition:
                    positionJson.put("lat", mc.getLocation().latitude);
                    positionJson.put("lon", mc.getLocation().longitude);
                    break;
            }
            result =  positionJson.toString();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

    public void sendMessageToServer(MessageContainer mc){
        Client c = new Client(jsonToString(mc));
        Thread clientThread= new Thread(c);
        clientThread.start();
    }

    @Override
    public void callBackReturn() {
        container=gala.getContainer();

        reciclerAdapter = new RecyclerMessage(this,container);


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(true);
        commonList.setAdapter(reciclerAdapter);
        commonList.setLayoutManager(layoutManager);
    }

    class WaitTask extends AsyncTask<Void, Void, Void> {

        int timeWait = 2;
        int block = 0;
        protected void addSec(int block){
            timeWait = 2;
            this.block = block;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            asyncStart = true;
            if(!firstStart){
                relativeFloatButtons.setVisibility(View.VISIBLE);
                relativeFloatButtons.startAnimation(animationShowFab);
            }else {
                firstStart = false;
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                while (timeWait>0||block==1||menuToogle) {
                    Thread.sleep(1000);
                    if(!menuToogle)
                    timeWait--;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            asyncStart = false;
//            relativeFloatButtons.setVisibility(View.VISIBLE);
            if(!textWrite) {
                relativeBackground.setBackgroundColor(getResources().getColor(R.color.rocket_full_transparent));
                relativeFloatButtons.startAnimation(animationHideFab);
            }

        }
    }
}
