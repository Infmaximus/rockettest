package com.infmaximus.rocketbank;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by InfMaximus on 17.07.2016.
 */
public class Client implements Runnable {
    private Socket socket;
    static final int SERVERPORT = 8080;
    static final String SERVERIP = "127.0.0.1";
    String messageText = "";

    public Client(String messageText){
        this.messageText = messageText;
    }

    @Override
    public void run() {
        try {
            InetAddress serverAddr = InetAddress.getByName(SERVERIP);
            Log.d("LOG_TAG", "client connect");
            socket = new Socket(serverAddr, SERVERPORT);
            sendNewMessage(messageText);
        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void sendNewMessage(String message){
        PrintWriter out;
        try {
            out = new PrintWriter(new BufferedWriter(
            new OutputStreamWriter(socket.getOutputStream())), true);
            out.println(message);
            Log.d("LOG_TAG","client send "+message);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
