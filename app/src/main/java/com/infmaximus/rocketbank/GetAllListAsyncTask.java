package com.infmaximus.rocketbank;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by InfMaximus on 19.07.2016.
 */
public class GetAllListAsyncTask extends AsyncTask<String,Integer,String>{

    private Context context;
    private SocketChannel socketChannel;

    private ProgressDialog pd;
    static final int SERVERPORT = 8080;
    static final String SERVERIP = "127.0.0.1";
    private ArrayList<MessageContainer> container = new ArrayList<>();

    public GetAllListAsyncTask(Context context){
        this.context = context;
    }

    public ArrayList<MessageContainer> getContainer(){
        return container;
    }

    interface Callback{
        void callBackReturn();
    }
    Callback myCallback;
    void registerCallBack(Callback callback){
        this.myCallback = callback;
    }

    private void transformToMC(String info){
        try {

            JSONArray jsonArray = new JSONArray(info);
            for(int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                MessageContainer mc = null;
                if(jsonObject.has(WorkDB.keyType)) {
                    String text;
                    Uri uri;
                    double lat = 0;
                    double lon = 0;
                    long date;
                    Calendar cal = Calendar.getInstance();
                    int type = jsonObject.getInt(WorkDB.keyType);
                    if (jsonObject.has(WorkDB.keyDate)) {
                        date = jsonObject.getLong(WorkDB.keyDate);
                        cal.setTimeInMillis(date);
                    }
                    switch (type) {
                        case 1:
                            if (jsonObject.has(WorkDB.keyText)) {
                                text = jsonObject.getString(WorkDB.keyText);
                                mc = new MessageContainer(type,text,cal);
                            }

                            break;
                        case 4:
                            if (jsonObject.has(WorkDB.keyUri)) {
                                uri = Uri.parse(jsonObject.getString(WorkDB.keyUri));
                                mc = new MessageContainer(type,uri,cal);
                            }
                            break;
                        case 3:
                            if (jsonObject.has(WorkDB.keyLat)) {
                                lat = jsonObject.getDouble(WorkDB.keyLat);
                            }
                            if (jsonObject.has(WorkDB.keyLat)) {
                                lon = jsonObject.getDouble(WorkDB.keyLon);
                            }
                            LatLng latLng = new LatLng(lat,lon);
                            mc = new MessageContainer(type,latLng,cal);
                            break;
                    }

                }
                if(mc!=null){
                    container.add(mc);
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        pd = new ProgressDialog(context);
        pd.setMessage("Идет загрузка");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        pd.dismiss();
        myCallback.callBackReturn();
    }

    @Override
    protected String doInBackground(String... params) {
        String atResult ="";
        try {
            String result = "";
            socketChannel = SocketChannel.open();
            socketChannel.configureBlocking(false);
            socketChannel.connect(new InetSocketAddress(SERVERIP, SERVERPORT));
            while (!socketChannel.finishConnect()) {
                //wait connection
            }
            if(socketChannel.isConnected()) {
                ByteBuffer buf = ByteBuffer.allocate("GET_MY_LIST".getBytes().length);
                buf.clear();
                buf.put("GET_MY_LIST".getBytes());
                buf.flip();
                socketChannel.write(buf);

                buf.clear();
            }

            boolean messageRecive = false;
            int lenghtPackage = 8192;
            int count;
            while (!messageRecive){
                ByteBuffer bufferA = ByteBuffer.allocate(lenghtPackage);
                while ((count = socketChannel.read(bufferA)) > 0){

                    bufferA.flip();
                    byte[] bytes = new byte[bufferA.remaining()];
                    bufferA.get(bytes);
                    String packet = new String(bytes,"UTF-8");
                    result+=packet;
                    messageRecive = true;
                }
            }

            if(!result.equals("empty")) {
                Log.d("LOG_TAG", "client get result " + result);
                transformToMC(result.replace("\\",""));
            }
            else
                Log.d("LOG_TAG","client get result empty");
        } catch (Exception e) {
            e.printStackTrace();
        }


        return atResult;
    }
}
