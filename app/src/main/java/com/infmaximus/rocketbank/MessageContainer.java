package com.infmaximus.rocketbank;

import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;

import com.google.android.gms.maps.model.LatLng;

import java.sql.Time;
import java.util.Calendar;

/**
 * Created by InfMaximus on 12.07.2016.
 */
public class MessageContainer {

    private int typeContainer;
    public final static int typeText = 1;
    public final static int typeImage = 2;
    public final static int typePosition = 3;
    public final static int typeImageUri = 4;

    private String textMessage = "";

    private Drawable bitmapMessage;

    private Uri bitmapMessageUri;

    private LatLng locationpMessage;

    private Calendar timeMessage;

    public LatLng getLocation() {
        return locationpMessage;
    }

    public Drawable getBitmapMessage() {
        return bitmapMessage;
    }

    public String getTextMessage() {
        return textMessage;
    }

    public int getTypeContainer() {
        return typeContainer;
    }

    public Uri getBitmapMessageUri(){
        return bitmapMessageUri;
    }

    public Calendar getTimeMessage(){
        return timeMessage;
    }

    public MessageContainer(int typeContainer, String textMessage,Calendar timeMessage){
        this.typeContainer = typeContainer;
        this.textMessage = textMessage;
        this.timeMessage = timeMessage;
    }

    public MessageContainer(int typeContainer, Drawable bitmapMessage,Calendar timeMessage){
        this.typeContainer = typeContainer;
        this.bitmapMessage = bitmapMessage;
        this.timeMessage = timeMessage;
    }

    public MessageContainer(int typeContainer, Uri bitmapMessageUri,Calendar timeMessage){
        this.typeContainer = typeContainer;
        this.bitmapMessageUri = bitmapMessageUri;
        this.timeMessage = timeMessage;
    }

    public MessageContainer(int typeContainer, LatLng locationpMessage,Calendar timeMessage){
        this.typeContainer = typeContainer;
        this.locationpMessage = locationpMessage;
        this.timeMessage = timeMessage;
    }

}
